       IDENTIFICATION DIVISION. 
       PROGRAM-ID. READ5.
       AUTHOR. Fhanglerr.
       ENVIRONMENT DIVISION. 

       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT 80-INPUT-FILE ASSIGN TO "MEMBER5.DAT"
              ORGANIZATION IS SEQUENTIAL
              FILE STATUS IS WS-INPUT-FILE-STATUS.

       DATA DIVISION. 
       FILE SECTION. 
       FD  80-INPUT-FILE
           BLOCK CONTAINS 0 RECORDS.
       01  100-INPUT-RECORD. 
           05 MEMBER-ID          PIC   X(5).
           05 MEMBER-NAME        PIC   X(20).
           05 MEMBER-SELECTED    PIC   9(1).
           05 MEMBER-GENDER      PIC   X(1).
           05 FILLER             PIC   X(53).

       WORKING-STORAGE SECTION.
       01  WS-INPUT-FILE-STATUS  PIC   X(2).
           88 FILE-SUCCESS                   VALUE '00'.
           88 FILE-AT-END                    VALUE '10'.
       01  WS-INPUT-COUNT        PIC   9(7)  VALUE ZERO.

       01  WS-MALE               PIC   9(7)  VALUE ZERO.
       01  WS-FEMALE             PIC   9(7)  VALUE ZERO.
       01  WS-CHOICE1            PIC   9(7)  VALUE ZERO.
       01  WS-CHOICE2            PIC   9(7)  VALUE ZERO.

       PROCEDURE DIVISION.
       0000-MAIN-PROGRAM.
           PERFORM 1000-INITIAL THRU 1000-EXIT.
           PERFORM 2000-PROCESS THRU 2000-EXIT
           PERFORM 3000-END THRU 3000-EXIT.
           GOBACK.

       1000-INITIAL.
           OPEN INPUT 80-INPUT-FILE
           IF NOT FILE-SUCCESS 
              DISPLAY "FILE NOT FOUND!!!"
              STOP RUN 
           END-IF 
           
           DISPLAY WS-INPUT-FILE-STATUS 
           
           PERFORM 8000-READ THRU 8000-EXIT.

       1000-EXIT.
           EXIT.

       2000-PROCESS.
           PERFORM UNTIL FILE-AT-END
                   DISPLAY WS-INPUT-FILE-STATUS
                   DISPLAY 100-INPUT-RECORD
                   ADD 1 TO WS-INPUT-COUNT
                   PERFORM 2100-COUNT-GENDER THRU 2100-EXIT 
                   PERFORM 2200-COUNT-CHOICE THRU 2200-EXIT  
                   PERFORM 8000-READ THRU 8000-EXIT
           END-PERFORM.

       2000-EXIT.
           EXIT.

       2100-COUNT-GENDER.
           IF MEMBER-GENDER = 'M'
              ADD 1 TO WS-MALE 
           ELSE
              ADD 1 TO WS-FEMALE 
           END-IF.

       2100-EXIT.
           EXIT.

       2200-COUNT-CHOICE.
           IF MEMBER-SELECTED = '0'
              ADD 1 TO WS-CHOICE1  
           ELSE
              ADD 1 TO WS-CHOICE2  
           END-IF.

       2200-EXIT.
           EXIT.

       3000-END.
           CLOSE 80-INPUT-FILE
           DISPLAY WS-INPUT-FILE-STATUS
           DISPLAY "READ : " WS-INPUT-COUNT
           DISPLAY "MALE : " WS-MALE 
           DISPLAY "FEMALE : " WS-FEMALE.
           DISPLAY "CHOICE 1 : " WS-CHOICE1  
           DISPLAY "CHOICE 2 : " WS-CHOICE2.

       3000-EXIT.
           EXIT.

       8000-READ.
           READ 80-INPUT-FILE.

       8000-EXIT.
           EXIT.
